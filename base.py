#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import subprocess

import MySQLdb

host="YOURHOST"
port=3306
user="USERNAME"
passwd="PASSWORD"
# db="example"

def get_connect(db):
    return MySQLdb.connect(host=host, port=port,
                           user=user, passwd=passwd,
                           db=db,
                           local_infile=1, charset='utf8')

def run_sql_script(db, file_path):
    with open(file_path) as stdin, open(os.devnull, 'w') as stdout:
        subprocess.call(["mysql", "-h", host, "-P", str(port), "-u", user, "--password={passwd}".format(passwd=passwd), db],
                        stdin=stdin, stdout=stdout, stderr=stdout)
