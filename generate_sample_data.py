#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import random
import datetime

import base

random.seed(10)

def load_data_from_file(cursor, file_path, table_name, columns):
    q = "LOAD DATA LOCAL INFILE 'data/{file_path}' INTO TABLE {table_name} FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' ({columns})".format(file_path=file_path, table_name=table_name, columns=columns)
    cursor.execute(q)

def load_offices(cursor):
    load_data_from_file(cursor, 'offices.csv', 'offices', 'name, email, postalcode')
    
def load_clients(cursor):
    load_data_from_file(cursor, 'clients.csv', 'clients', 'family_name, given_name, email, postalcode')

def load_parts(cursor):
    load_data_from_file(cursor, 'parts.csv', 'parts', 'name, price, stock_quantity, productivity')

def condition(office, client, part):
    """
    3000円以上を購入：承太郎,ディオ
    住んでいる都道府県の拠点からのみ購入：ジョージ, ジョナサン, 丈助, ジョルノ
    """
    # client_given_name = unicode(client[2])
    client_id = client[0]
    
    part_price = part[1]
    # if [unicode(u'承太郎'), unicode(u'ディオ')].count(client_given_name) == 0:
    if [3, 6].count(client_id) == 0:
        if part_price >= 3000:
            return False

    client_postalcode = int(client[-1]) // 100000
    office_postalcode = int(office[-1]) // 100000
    # if [u'ジョージ', u'ジョナサン', u'丈助', u'ジョルノ'].count(client_given_name) > 0:
    if [1, 2, 7, 8].count(client_id) > 0:
        if client_postalcode != office_postalcode:
            return False

    return True

def get_lists(cursor):
    cursor.execute("SELECT id, postalcode FROM offices")
    offices = cursor.fetchall()
    cursor.execute("SELECT id, family_name, given_name, postalcode FROM clients")
    clients = cursor.fetchall()
    # print clients
    cursor.execute("SELECT id, price FROM parts")
    parts = cursor.fetchall()
    return offices, clients, parts

def rand_quantity():
    return random.randint(1, 1000)

sdate = datetime.date(2000, 1, 1)
edate = datetime.date(2021, 1, 1)
max_days=(edate-sdate).days
def rand_date():
    return sdate + datetime.timedelta(days=random.randrange(max_days))

def rand_date_added(date):
    return date + datetime.timedelta(days=random.randrange(7))

def generate_order(offices, clients, parts):
    while True:
        o = random.choice(offices)
        c = random.choice(clients)
        p = random.choice(parts)
        if condition(o, c, p):
            break;

    quantity = rand_quantity()
    ordered_date = rand_date()
    estimated_delivery_date = rand_date_added(ordered_date)
    delivered_date = estimated_delivery_date

    return o[0], c[0], p[0], quantity, ordered_date, estimated_delivery_date, delivered_date

def generate_orders(cursor, num_orders):
    offices, clients, parts = get_lists(cursor)
    values = []
    for i in range(num_orders):
        values.append(generate_order(offices, clients, parts))

    # print values
    cursor.executemany(
        """INSERT INTO orders (office, client, part, quantity, ordered_date, estimated_delivery_date, delivered_date)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
        """,
        values
    )

def main(database, num_orders):
    connect = base.get_connect(database)
    cursor = connect.cursor()
    load_offices(cursor)
    load_clients(cursor)
    load_parts(cursor)
    generate_orders(cursor, num_orders)
    connect.commit()

if __name__=="__main__":
    main(num_orders=int(sys.argv[1]))
