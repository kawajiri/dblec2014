CREATE INDEX offices_index on offices (id);
CREATE INDEX clients_index on clients (id);
CREATE INDEX parts_index on parts (id);
CREATE INDEX orders_index on orders (id);

CREATE INDEX parts_price_index on parts (price);
CREATE INDEX offices_postalcode_index on orders (postalcode);
CREATE INDEX clients_postalcode_index on clients (postalcode);
CREATE INDEX ordered_date_index on orders (ordered_date);
CREATE INDEX delivered_date_index on orders (delivered_date);
