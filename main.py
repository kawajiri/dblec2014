#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import pickle

import base
import generate_sample_data


def reset_tables():
    base.run_sql_script("example", "sql/create_tables.sql")

def customers_bought_expensive_parts():
    base.run_sql_script("example", "sql/customers_bought_expensive_parts.sql")

def customers_bought_locally():
    base.run_sql_script("example", "sql/customers_bought_locally.sql")

def average_nouhin():
    base.run_sql_script("example", "sql/average_nouhin.sql")

def create_indices():
    base.run_sql_script("example", "sql/create_indices.sql")

def check_repeat(func, arr):
    for i in range(10):
        tic = time.time()
        func()
        toc = time.time()
        arr.append(toc - tic)

def measure_performance(expen, local, nouhin):
    check_repeat(customers_bought_expensive_parts, expen)
    check_repeat(customers_bought_locally, local)
    check_repeat(average_nouhin, nouhin)

def measure_all(file_path, indices=False):
    num = 64
    nums = []
    expensive_all = []
    local_all = []
    nouhin_all = []
    for i in range(10):
        print num

        expen = []
        local = []
        nouhin = []

        reset_tables()
        generate_sample_data.main("example", num)
        if indices:
            create_indices()
        measure_performance(expen, local, nouhin)

        expensive_all.append(expen)
        local_all.append(local)
        nouhin_all.append(nouhin)
        nums.append(num)

        num = num * 2

    obj = {"nums":nums,
           "exp":expensive_all,
           "lcl":local_all,
           "nhn":nouhin_all}
    pickle.dump(obj, open(file_path, "w"))

if __name__ == "__main__":
    if sys.argv[2] == 'i':
        measure_all(sys.argv[1], indices=True)
    else:
        measure_all(sys.argv[1], indices=False)
